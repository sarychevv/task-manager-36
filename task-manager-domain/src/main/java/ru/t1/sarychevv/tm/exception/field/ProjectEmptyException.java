package ru.t1.sarychevv.tm.exception.field;

public final class ProjectEmptyException extends AbstractFieldException {

    public ProjectEmptyException() {
        super("Error! Project is empty...");
    }

}
