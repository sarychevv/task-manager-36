package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sarychevv.tm.dto.request.user.UserLoginRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserProfileRequest;
import ru.t1.sarychevv.tm.dto.response.user.UserLoginResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserLogoutResponse;
import ru.t1.sarychevv.tm.marker.SoapCategory;

public class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final String login = "admin";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        token = authEndpoint.login(request).getToken();
    }

    @Test
    @Category(SoapCategory.class)
    public void testLogin() {
        @NotNull final String login = "admin";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(request);
        Assert.assertEquals(true, userLoginResponse.getSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void testProfile() {
        @NotNull final String login = "admin";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(login);
        userLoginRequest.setPassword(password);
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(userLoginRequest);
        @NotNull final UserProfileRequest request = new UserProfileRequest(userLoginResponse.getToken());
        authEndpoint.profile(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testLogout() {
        @NotNull final String login = "admin";
        @NotNull final String password = "admin";
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(login);
        userLoginRequest.setPassword(password);
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(userLoginRequest);
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(userLoginResponse.getToken());
        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(request);
        Assert.assertEquals(true, userLogoutResponse.getSuccess());
    }

}
