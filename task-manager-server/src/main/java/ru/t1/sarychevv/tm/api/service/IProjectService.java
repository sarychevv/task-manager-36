package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(@Nullable String name, @Nullable String description);

}
